﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    public class CPUEnemy
    {
        private bool hasRun;

        public CPUEnemy()
        {
            hasRun = false;
        }

        public void Update(List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits, TurnState turnState)
        {
            if (turnState != TurnState.enemyTurn)
            {
                return;
            }

            if (hasRun)
            {
                return;
            }

            hasRun = true;

            Vector2 playerBasePosition = new Vector2(-1, -1);
            foreach (Entity entity in myUnits)
            {
                if (entity is Building)
                {
                    playerBasePosition = entity.getPosition();
                }
            }

            if (playerBasePosition.X == -1 || playerBasePosition.Y == -1)
            {
                return;
            }

            foreach (Entity unit in myUnits)
            {
                Unit myUnit;
                if (unit is Unit)
                {
                    myUnit = (Unit)unit;
                }
                else
                {
                    continue;
                }
                List<Unit> currentList = CheckNear(myUnit, enemyUnits);
                if (currentList.Count > 1)
                {
                    foreach (Unit enemy in currentList)
                    {
                        MoveTowards(enemy, unit.getPosition(), allUnits);
                    }
                }
            }

            foreach (Entity badGuy in enemyUnits)
            {
                Unit uBadGuy;
                if (badGuy is Unit)
                {
                    uBadGuy = (Unit)badGuy;
                }
                else
                {
                    continue;
                }
                if (uBadGuy.MoveLeft() > 0)
                {
                    MoveTowards(uBadGuy, playerBasePosition, allUnits);
                }
            }

            foreach (Entity badGuy in enemyUnits)
            {
                Unit uBadGuy;
                if (badGuy is Unit)
                {
                    uBadGuy = (Unit)badGuy;
                }
                else
                {
                    continue;
                }
                if (!uBadGuy.HasAttacked())
                {
                    List<Entity> possibleAttacks = AttackOptions(uBadGuy, uBadGuy.AttackRange(), uBadGuy.getPosition(), myUnits);
                    if (possibleAttacks.Count != 0)
                    {
                        int minHealth = -1;
                        Entity weakest = possibleAttacks.ElementAt(0);
                        foreach (Entity posAtk in possibleAttacks)
                        {
                            if (posAtk.GetHealth() < minHealth || minHealth == -1)
                            {
                                weakest = posAtk;
                            }
                        }
                        uBadGuy.EnemyAttack((int)weakest.getPosition().X, (int)weakest.getPosition().Y, myUnits);
                    }
                }
            }
        }

        public void ResetRun()
        {
            hasRun = false;
        }

        private List<Unit> CheckNear(Unit unit, List<Entity> enemyUnits)
        {
            List<Unit> list = new List<Unit>();
            Vector2 unitCoords = unit.getPosition();
            foreach (Entity entity in enemyUnits)
            {
                Unit enemy;
                if (entity is Unit)
                {
                    enemy = (Unit)entity;
                }
                else
                {
                    continue;
                }
                Vector2 entityCoords = entity.getPosition();
                if (unitCoords.X >= entityCoords.X - 64 && unitCoords.X <= entityCoords.X + 64)
                {
                    if (unitCoords.Y >= entityCoords.Y - 64 && unitCoords.Y <= entityCoords.Y + 64)
                    {
                        list.Add(enemy);
                    }
                }
            }
            return list;
        }

        private List<Entity> AttackOptions(Unit unit, int range, Vector2 coords, List<Entity> myUnits)
        {
            List<Entity> list = new List<Entity>();
            List<Vector2> check = new List<Vector2>();
            if (range <= 0)
            {
                return list;
            }
            check.Add(new Vector2(coords.X + 32, coords.Y));
            check.Add(new Vector2(coords.X - 32, coords.Y));
            check.Add(new Vector2(coords.X, coords.Y + 32));
            check.Add(new Vector2(coords.X, coords.Y - 32));
            foreach (Vector2 v in check)
            {
                if (v.X >= 0 && v.Y >= 0)
                {
                    foreach (Entity entity in myUnits)
                    {
                        if (entity.getPosition().Equals(v))
                        {
                            list.Add(entity);
                        }
                    }
                }
            }

            if (range > 1)
            {
                foreach (Entity e in AttackOptions(unit, range - 1, new Vector2(coords.X, coords.Y + 32), myUnits))
                {
                    list.Add(e);
                }
                foreach (Entity v in AttackOptions(unit, range - 1, new Vector2(coords.X, coords.Y - 32), myUnits))
                {
                    list.Add(v);
                }
                foreach (Entity e in AttackOptions(unit, range - 1, new Vector2(coords.X + 32, coords.Y), myUnits))
                {
                    list.Add(e);
                }
                foreach (Entity e in AttackOptions(unit, range - 1, new Vector2(coords.X - 32, coords.Y), myUnits))
                {
                    list.Add(e);
                }
            }
            return list;
        }

        private void MoveTowards(Unit unit, Vector2 coords, List<Entity> allUnits)
        {
            List<Vector2> list = GetMovementRange(unit.MoveLeft(), unit.getPosition(), allUnits);
            int min = 0;
            min += (int)Math.Abs(unit.getPosition().X - coords.X);
            min += (int)Math.Abs(unit.getPosition().Y - coords.Y);
            Vector2 closest = unit.getPosition();
            foreach (Vector2 vec in list)
            {
                int space = 0;
                space += (int) Math.Abs(vec.X - coords.X);
                space += (int) Math.Abs(vec.Y - coords.Y);
                if (space < min)
                {
                    min = space;
                    closest = vec;
                }
            }
            unit.EnemyMove((int) closest.X, (int) closest.Y);
        }


        private List<Vector2> GetMovementRange(int range, Vector2 coords, List<Entity> allUnits)
        {
            List<Vector2> list = new List<Vector2>();
            List<Vector2> check = new List<Vector2>();
            if (range <= 0)
            {
                return list;
            }
            check.Add(new Vector2(coords.X + 32, coords.Y));
            check.Add(new Vector2(coords.X - 32, coords.Y));
            check.Add(new Vector2(coords.X, coords.Y + 32));
            check.Add(new Vector2(coords.X, coords.Y - 32));
            foreach (Vector2 v in check)
            {
                if (v.X >= 0 && v.Y >= 0)
                {
                    bool matches = false;
                    foreach (Entity entity in allUnits)
                    {
                        if (entity.getPosition().Equals(v))
                        {
                            matches = true;
                        }
                    }
                    if (!matches)
                    {
                        list.Add(v);
                    }
                }
            }

            List<Vector2> copy = new List<Vector2>();
            foreach (Vector2 v in list)
            {
                copy.Add(v);
            }
            if (range > 1)
            {
                foreach (Vector2 c in copy)
                {
                    foreach (Vector2 v in GetMovementRange(range - 1, c, allUnits))
                    {
                        list.Add(v);
                    }
                }

            }
            return list;
        }
    }
}
