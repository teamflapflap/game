﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Lidgren.Network;

namespace game
{
    class Connection
    {
        NetPeerConfiguration config;
        NetServer server;
        NetClient client;
        NetIncomingMessage msg;
        NetOutgoingMessage response;
        NetOutgoingMessage sendMsg;
        NetPeerConfiguration clientConfig;
        private Game1 game;
        NetConnection clientConnection;

        public Connection(Game1 game)
        {
            this.game = game;
        }

        //converts object to byte array
        private static byte[] ObjectToByteArray(Object data)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, data);
                return ms.ToArray();
            }
        }

        //converts byte array back to object
        private static Object ByteArrayToObject(byte[] data)
        {
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                ms.Write(data, 0, data.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var obj = bf.Deserialize(ms);
                return obj;
            }
        }

        //encodes message and sends to other connected computer
        public void SendData(Object data)
        {
            if (game.clientConnected) sendMsg = server.CreateMessage();
            else if (game.serverConnected) sendMsg = client.CreateMessage();
            byte[] sendData = ObjectToByteArray(data);
            sendMsg.Write(sendData);
            if (game.clientConnected) server.SendMessage(sendMsg, clientConnection, NetDeliveryMethod.ReliableOrdered);
            else if (game.serverConnected) client.SendMessage(sendMsg, NetDeliveryMethod.ReliableOrdered);
        }

        //starts server
        public void StartServer()
        {
            config = new NetPeerConfiguration("Test");
            config.Port = 14242;

            server = new NetServer(config);
            server.Start();
        }

        //checks for any new messages from client
        public void CheckServer()
        {
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        byte[] dataBytes = msg.ReadBytes(msg.LengthBytes);
                        Object data = ByteArrayToObject(dataBytes);

                        if (data is string)
                        {
                            string fullText = (string)data;
                            Console.WriteLine(fullText);
                            string[] textArray = fullText.Split(',');
                            if (textArray[0] == "send_hi_back_please")
                            {
                                game.clientConnected = true;
                                response = server.CreateMessage();
                                byte[] preppedResponse = ObjectToByteArray("hi");
                                response.Write(preppedResponse);
                                clientConnection = msg.SenderConnection;
                                server.SendMessage(response, clientConnection, NetDeliveryMethod.ReliableOrdered);
                            }
                            else
                            {
                                game.GetMessage(fullText);
                            }
                        }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        break;
                    default:
                        Console.WriteLine("Unhandled type: " + msg.MessageType);
                        break;
                }
                server.Recycle(msg);
            }
        }

        //checks for any new messages form server
        public void CheckClient()
        {
            while ((msg = client.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        byte[] dataBytes = msg.ReadBytes(msg.LengthBytes);
                        Object data = ByteArrayToObject(dataBytes);

                        if (data is string)
                        {
                            string fullText = (string)data;
                            Console.WriteLine(fullText);
                            string[] textArray = fullText.Split(',');
                            if (textArray[0] == "hi")
                            {
                                game.serverConnected = true;
                            }
                            else if (textArray[0] == "load")
                            {
                                game.mapToLoad = textArray[1];
                                game.CurrentState = GameState.Play;
                                game.StopMusic();
                            }
                            else
                            {
                                game.GetMessage(fullText);
                            }
                        }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        break;
                    default:
                        Console.WriteLine("Unhandled type: " + msg.MessageType);
                        break;
                }
                client.Recycle(msg);
            }
        }

        //starts client and sends a message to server to verify connection
        public bool Connect(string ip)
        {
            clientConfig = new NetPeerConfiguration("Test");
            client = new NetClient(clientConfig);
            client.Start();
            try
            {
                client.Connect(host: ip, port: 14242);
            }
            catch(Exception e)
            {
                client.Disconnect("disconnecting");
                Console.WriteLine(e.Data.ToString());
                return true;
            }
            
            Thread.Sleep(500);
            sendMsg = client.CreateMessage();
            byte[] preppedMsg = ObjectToByteArray("send_hi_back_please");
            sendMsg.Write(preppedMsg);
            client.SendMessage(sendMsg, NetDeliveryMethod.ReliableOrdered);
            Thread.Sleep(500);
            CheckClient();
            return false;
        }
    }
}
