﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.Entities
{
    class Archer : Unit
    {
        public Archer(Vector2 position, bool owned, Texture2D tex, Texture2D moveTex, Texture2D atkTex, int x, int y) : base(position, owned, tex, moveTex, atkTex, x, y)
        {
            health = 6;
            atk = 3;
            atkRange = 2;
            moveRange = 2;
            moveLeft = moveRange;
        }
    }
}
