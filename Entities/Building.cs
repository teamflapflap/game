﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class Building : Entity, IEntity
    {

        public Building(Vector2 position, bool owned, Texture2D tex) : base(position, owned, tex)
        {
            health = 10;
        }

        public override bool Attack(int x, int y)
        {
            return false;
        }

        public override void DisplayOptions(bool owned, SpriteBatch batch, Camera2D cam, List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits)
        {
            
        }

        public override void EnemyAttack(int x, int y, List<Entity> myUnits)
        {
            throw new NotImplementedException();
        }

        public override Vector2 EnemyMove(int x, int y)
        {
            throw new NotImplementedException();
        }

        public override bool Move(int x, int y)
        {
            return false;
        }

        public override void TurnReset()
        {
            return;
        }
    }
}
