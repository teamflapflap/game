﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using System.Collections.Generic;

namespace game
{
    public enum Owner { Player1, Player2}

    interface IEntity
    {
        void DisplayOptions(bool owned, SpriteBatch batch, Camera2D cam, List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits);
    }

    public abstract class Entity : IEntity
    {
        protected Vector2 position;
        protected int health;
        public bool owned;
        protected enum State { normal, selected };
        protected State state;
        protected Texture2D tex;
        private MouseState lastState;
        private bool tookDamage;
        private int damageTaken;

        public Entity(Vector2 position, bool owned, Texture2D tex)
        {
            this.position = position;
            this.owned = owned;
            this.tex = tex;
            state = State.normal;
            tookDamage = false;
            damageTaken = 0;
        }

        public abstract void DisplayOptions(bool owned, SpriteBatch batch, Camera2D cam, List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits);

        public abstract bool Move(int x, int y);

        public abstract bool Attack(int x, int y);

        public abstract Vector2 EnemyMove(int x, int y);

        public abstract void EnemyAttack(int x, int y, List<Entity> myUnits);

        public abstract void TurnReset();

        public string Update(SpriteBatch batch, Camera2D cam, List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits, TurnState turnState)
        {
            string updateString = "";
            if (tex == null)
            {
                return updateString;
            }
            batch.Draw(tex, cam.WorldToScreen(this.position), Color.White);
            MouseState mouseState = Mouse.GetState();
            Vector2 mouseCoords = cam.ScreenToWorld(new Vector2(mouseState.X, mouseState.Y));
            if (turnState == TurnState.playerTurn)
            {
                if (lastState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                {
                    int x = (int)mouseCoords.X;
                    int y = (int)mouseCoords.Y;
                    if (state == State.normal)
                    {
                        if (x >= position.X && x <= position.X + 32 && y >= position.Y && y <= position.Y + 32)
                        {
                            state = State.selected;
                        }
                    }
                    else if (state == State.selected && owned)
                    {
                        int newX = x - (x % 32);
                        int newY = y - (y % 32);
                        if (Move(newX, newY))
                        {
                            updateString = "M," + newX + "," + newY;
                        }
                        if (Attack(newX, newY))
                        {
                            updateString = "A," + newX + "," + newY; 
                        }
                        state = State.normal;
                    }
                }
                DisplayOptions(owned, batch, cam, allUnits, myUnits, enemyUnits);
                lastState = mouseState;
            }
            return updateString;
        }

        public int GetHealth()
        {
            return health;
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public void TakeDamage(int atk)
        {
            health -= atk;
            tookDamage = true;
            damageTaken = atk;
        }

        public int DamageTaken()
        {
            int temp = damageTaken;
            damageTaken = 0;
            return temp;
        }

        public bool TookDamage()
        {
            bool temp = tookDamage;
            tookDamage = false;
            return temp;
        }

        public bool isOwned()
        {
            return owned;
        }
    }
}
