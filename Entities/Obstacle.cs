﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace game.Entities
{
    class Obstacle : Unit
    {
        
        public Obstacle(Vector2 position, bool owned, Texture2D tex) : base(position, owned, tex)
        {
            health = 1;
        }

        public override bool Attack(int x, int y)
        {
            return false;
        }

        public override void DisplayOptions(bool owned, SpriteBatch batch, Camera2D cam, List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits)
        {
            
        }

        public override bool Move(int x, int y)
        {
            return false;
        }

        public override void TurnReset()
        {
            
        }
    }
}
