﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.Entities
{
    class Swordsman : Unit
    {
        public Swordsman(Vector2 position, bool owned, Texture2D tex, Texture2D moveTex, Texture2D atkTex, int x, int y) : base(position, owned, tex, moveTex, atkTex, x, y)
        {
            health = 10;
            atk = 4;
            atkRange = 1;
            moveRange = 2;
            moveLeft = moveRange;
        }
    }
}
