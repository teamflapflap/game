﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.Entities
{
    class Tank : Unit
    {
        public Tank(Vector2 position, bool owned, Texture2D tex, Texture2D moveTex, Texture2D atkTex, int x, int y) : base(position, owned, tex, moveTex, atkTex, x, y)
        {
            health = 16;
            atk = 1;
            atkRange = 1;
            moveRange = 1;
            moveLeft = moveRange;
        }
    }
}
