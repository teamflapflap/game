﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using MonoGame.Extended;


namespace game
{
    abstract class Unit : Entity, IEntity
    {
        protected int atk;
        protected int atkRange;
        protected int moveRange;
        protected int moveLeft;
        protected bool hasAttacked;
        private int maxX;
        private int maxY;
        protected Texture2D moveTex;
        protected Texture2D atkTex;
        protected List<Vector2> validMoves;
        protected List<Entity> validAttacks;

        public Unit(Vector2 position, bool owned, Texture2D tex, Texture2D moveTex, Texture2D atkTex, int maxX, int maxY) : base(position, owned, tex)
        {
            this.moveTex = moveTex;
            this.atkTex = atkTex;
            hasAttacked = false;
            this.maxX = maxX;
            this.maxY = maxY;
        }

        protected Unit(Vector2 position, bool owned, Texture2D tex) : base(position, owned, tex)
        {
            this.position = position;
            this.owned = owned;
            this.tex = tex;
        }

        public int MoveLeft()
        {
            return moveLeft;
        }

        public bool HasAttacked()
        {
            return hasAttacked;
        }

        public int AttackRange()
        {
            return atkRange;
        }

        public override void TurnReset()
        {
            moveLeft = moveRange;
            hasAttacked = false;
        }


        override public void DisplayOptions(bool owned, SpriteBatch batch, Camera2D cam, List<Entity> allUnits, List<Entity> myUnits, List<Entity> enemyUnits)
        {
            if (state == State.selected && owned)
            {
                validMoves = DisplayMovementRange(batch, cam, moveLeft, position, allUnits);
                validAttacks = DisplayAttackRange(batch, cam, atkRange, position, enemyUnits);
            }
        }

        public override bool Move(int x, int y)
        {
            Vector2 vec = new Vector2(x, y);
            int space = 0;
            foreach (Vector2 move in validMoves)
            {
                if (vec.Equals(move))
                {
                    space += Math.Abs((int) position.X - (int) vec.X);
                    space += Math.Abs((int) position.Y - (int) vec.Y);
                    space /= 32;
                    moveLeft -= space;
                    position = vec;
                    return true;
                }
            }
            return false;
        }

        public override Vector2 EnemyMove(int x, int y)
        {
            Vector2 vec = new Vector2(x, y);
            int space = 0;
            space += Math.Abs((int)position.X - (int)vec.X);
            space += Math.Abs((int)position.Y - (int)vec.Y);
            space /= 32;
            moveLeft -= space;
            position = vec;
            return vec;
        }


        public override bool Attack(int x, int y)
        {
            Vector2 vec = new Vector2(x, y);
            foreach (Entity entity in validAttacks)
            {
                if (vec.Equals(entity.getPosition()))
                {
                    entity.TakeDamage(atk);
                    hasAttacked = true;
                    moveLeft = 0;
                    return true;
                }
            }
            return false;
        }

        public override void EnemyAttack(int x, int y, List<Entity> myUnits)
        {
            Vector2 vec = new Vector2(x, y);
            foreach (Entity ent in myUnits)
            {
                if (vec.Equals(ent.getPosition()))
                {
                    ent.TakeDamage(atk);
                    hasAttacked = true;
                    moveLeft = 0;
                    return;
                }
            }
        }

        List<Vector2> DisplayMovementRange(SpriteBatch batch, Camera2D cam, int range, Vector2 coords, List<Entity> allUnits)
        {
            List<Vector2> list = new List<Vector2>();
            List<Vector2> check = new List<Vector2>();
            if (range <= 0)
            {
                return list;
            }
            check.Add(new Vector2(coords.X + 32, coords.Y));
            check.Add(new Vector2(coords.X - 32, coords.Y));
            check.Add(new Vector2(coords.X, coords.Y + 32));
            check.Add(new Vector2(coords.X, coords.Y - 32));
            foreach (Vector2 v in check)
            {
                if (v.X >= 0 && v.Y >= 0 && v.X <= maxX - 32 && v.Y <= maxY - 32) // Add restraint to make sure it stays in the map
                {
                    bool matches = false;
                    foreach (Entity entity in allUnits)
                    {
                        if (entity.getPosition().Equals(v))
                        {
                            matches = true;
                        }
                    }
                    if (!matches)
                    {
                        list.Add(v);
                        batch.Draw(moveTex, cam.WorldToScreen(v), Color.White);
                    }
                }
            }

            List<Vector2> copy = new List<Vector2>();
            foreach (Vector2 v in list)
            {
                copy.Add(v);
            }
            if (range > 1)
            {
                foreach (Vector2 c in copy)
                {
                    foreach (Vector2 v in DisplayMovementRange(batch, cam, range - 1, c, allUnits))
                    {
                        list.Add(v);
                    }
                }
                
            }
            return list;
        }

        List<Entity> DisplayAttackRange(SpriteBatch batch, Camera2D cam, int range, Vector2 coords, List<Entity> enemyUnits)
        {
            // Have red squares appear
            List<Entity> list = new List<Entity>();
            List<Vector2> check = new List<Vector2>();
            if (range <= 0 || hasAttacked)
            {
                return list;
            }
            check.Add(new Vector2(coords.X + 32, coords.Y));
            check.Add(new Vector2(coords.X - 32, coords.Y));
            check.Add(new Vector2(coords.X, coords.Y + 32));
            check.Add(new Vector2(coords.X, coords.Y - 32));
            foreach (Vector2 v in check)
            {
                if (v.X >= 0 && v.Y >= 0)
                {
                    foreach (Entity entity in enemyUnits)
                    {
                        if (entity.getPosition().Equals(v))
                        {
                            list.Add(entity);
                            batch.Draw(atkTex, cam.WorldToScreen(v), Color.White);
                        }
                    }
                }
            }

            if (range > 1)
            {
                foreach (Entity e in DisplayAttackRange(batch, cam, range - 1, new Vector2(coords.X, coords.Y + 32), enemyUnits))
                {
                    list.Add(e);
                }
                foreach (Entity v in DisplayAttackRange(batch, cam, range - 1, new Vector2(coords.X, coords.Y - 32), enemyUnits))
                {
                    list.Add(v);
                }
                foreach (Entity e in DisplayAttackRange(batch, cam, range - 1, new Vector2(coords.X + 32, coords.Y), enemyUnits))
                {
                    list.Add(e);
                }
                foreach (Entity e in DisplayAttackRange(batch, cam, range - 1, new Vector2(coords.X - 32, coords.Y), enemyUnits))
                {
                    list.Add(e);
                }
            }
            return list;
        }
    }
}
