﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.ViewportAdapters;

namespace game
{
    //declare GameState enum so we can switch between menu and game states
    public enum GameState { Menu, Play, InternetPlay, win, lose };
    public enum TurnState { playerTurn, enemyTurn };

    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        public GameState gameState;
        public Owner currentPlayer;
        public bool serverStarted;
        public bool clientStarted;
        public bool serverConnected;
        public bool clientConnected;
        private Menu menu;
        private GameScene gameScene;
        private ViewportAdapter viewportAdapter;
        private Connection connection;
        private float secondCount;
        public string mapToLoad;
        private SoundEffect loseMusic;
        public SoundEffectInstance loseI;
        private SoundEffect winMusic;
        public SoundEffectInstance winI;

        /* the code in the get and set blocks will run whenever we use this to modify gameScene
         * this way we can ensure the Initialize and LoadContent methods are ran before switching to
         * gameScene
         * it is also used to set the win / lose menuState
         */ 
        public GameState CurrentState
        {
            get { return gameState; }
            set
            {
                if (value == GameState.Play)
                {
                    InitializeGameScene();
                    gameState = value;
                }
                if(value == GameState.win)
                {
                    winI.Play();
                    menu.menuState = MenuState.win;
                    gameState = GameState.Menu;
                }
                if (value == GameState.lose)
                {
                    loseI.Play();
                    menu.menuState = MenuState.lose;
                    gameState = GameState.Menu;
                }
            }
        }

        public Game1()
        {
            //send the current Game1 instance to the scene constructors so we can modify gameState
            menu = new Menu(this);
            gameState = GameState.Menu;
            serverStarted = false;
            clientStarted = false;
            serverConnected = false;
            clientConnected = false;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            connection = new Connection(this);
            secondCount = 0;
            mapToLoad = "";
        }

        //this method is the first to run after the constructor
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 1366;
            graphics.PreferredBackBufferHeight = 768;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            viewportAdapter = new BoxingViewportAdapter(Window, GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            //runs the proper initialize method depending on which scene is currently selected
            switch (gameState)
            {
                case GameState.Menu:
                    menu.Initialize();
                    break;

                case GameState.Play:
                    gameScene.Initialize(GraphicsDevice, viewportAdapter);
                    break;

                default:
                    break;
            }

            Window.AllowUserResizing = false;
            base.Initialize();
        }

        //this runs after Initialize
        protected override void LoadContent()
        {
            loseMusic = Content.Load<SoundEffect>("gameOver");
            loseI = loseMusic.CreateInstance();
            loseI.IsLooped = true;
            winMusic = Content.Load<SoundEffect>("winTheGame");
            winI = winMusic.CreateInstance();
            switch (gameState)
            {
                case GameState.Menu:
                    menu.LoadContent(Content, GraphicsDevice);
                    break;

                case GameState.Play:
                    gameScene.LoadContent(Content, GraphicsDevice);
                    break;

                default:
                    break;
            }
        }

        protected override void UnloadContent()
        {
        }

        //this runs repeatedly and before Draw
        protected override void Update(GameTime gameTime)
        {
            //check the client / server connection every second
            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            secondCount += deltaSeconds;
            if(secondCount > 0.01)
            {
                if(serverStarted) connection.CheckServer();
                if(clientStarted) connection.CheckClient();
                secondCount = 0;
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            switch (gameState)
            {
                case GameState.Menu:
                    menu.Update(GraphicsDevice);
                    break;

                case GameState.Play:
                    gameScene.Update(gameTime);
                    break;

                default:
                    break;
            }

            base.Update(gameTime);
        }

        public void GetMessage(String message)
        {
            gameScene.GetInternetMessage(message);
        }

        public void StopMusic()
        {
            menu.StopMusic();
        }

        //this runs repeatedly and after update
        protected override void Draw(GameTime gameTime)
        {

            switch (gameState)
            {
                case GameState.Menu:
                    menu.Draw(GraphicsDevice);
                    break;

                case GameState.Play:
                    gameScene.Draw(GraphicsDevice, gameTime);
                    break;

                default:
                    break;
            }

            base.Draw(gameTime);
        }

        //since using the "this" keyword is not permitted in the set block for the enum, we make it
        //call this method and use it here instead
        public void InitializeGameScene()
        {
            gameScene = new GameScene(this, currentPlayer);
            gameScene.Initialize(GraphicsDevice, viewportAdapter);
            gameScene.LoadContent(Content, GraphicsDevice);
        }

        public void StartServer()
        {
            connection.StartServer();
        }

        public bool ConnectToServer(string ip)
        {
            return connection.Connect(ip);
        }

        public void SendMessage(Object data)
        {
            connection.SendData(data);
        }
    }
}
