﻿using game.Entities;
using game.Indicators;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.Tiled;
using MonoGame.Extended.Tiled.Graphics;
using MonoGame.Extended.ViewportAdapters;
using System;
using System.Collections.Generic;
using System.IO;

namespace game
{
    public class GameScene
    {
        private SpriteBatch spriteBatch;
        private TiledMapRenderer renderer;
        private TiledMap map;
        private Camera2D camera;
        private Owner currentPlayer;
        private List<Entity> allUnits;
        private List<Entity> myUnits;
        private List<Entity> enemyUnits;
        private TurnState turnState;
        private TurnState previousTurnState;
        private CPUEnemy cpu;
        public SpriteFont font;
        private List<DamageIndicator> dgIlist;
        private List<DeathIndicator> dthIlist;
        private List<TurnOverIndicator> tOList;
        private bool hasMyBase;
        private bool hasOnlyMyBase;
        private bool hasEnemyBase;
        private bool hasOnlyEnemyBase;
        private Texture2D explode;
        private SoundEffect baseDestroyed;
        private SoundEffect unitDestroyed;
        private SoundEffect unitTakeDamage;
        private SoundEffect mainMusic;
        private SoundEffectInstance music;

        private Game1 game;

        public GameScene(Game1 game, Owner currentPlayer)
        {
            this.game = game;
            this.currentPlayer = currentPlayer;
            allUnits = new List<Entity>();
            myUnits = new List<Entity>();
            enemyUnits = new List<Entity>();
            this.turnState = TurnState.playerTurn;
            cpu = new CPUEnemy();
            dgIlist = new List<DamageIndicator>();
            dthIlist = new List<DeathIndicator>();
            tOList = new List<TurnOverIndicator>();
            hasMyBase = false;
            hasOnlyMyBase = true;
            hasEnemyBase = false;
            hasOnlyEnemyBase = true;
        }

        public void Initialize(GraphicsDevice graphicsDevice, ViewportAdapter viewportAdapter)
        {
            //we declare the camera we will use
            camera = new Camera2D(viewportAdapter);
        }

        public void LoadContent(ContentManager content, GraphicsDevice graphicsDevice)
        {
            //we declare the spriteBatch, map, and renderer we will use, and sets the initial
            //camera position
            spriteBatch = new SpriteBatch(graphicsDevice);
            map = content.Load<TiledMap>(game.mapToLoad);
            renderer = new TiledMapRenderer(graphicsDevice);
            camera.LookAt(new Vector2(map.WidthInPixels, map.HeightInPixels) * 0.5f);
            font = content.Load<SpriteFont>("ArialFont");
            Texture2D archerSymbol = content.Load<Texture2D>("archerSymbol");
            Texture2D badArcherSquare = content.Load<Texture2D>("badArcherSquare");
            Texture2D moveTex = content.Load<Texture2D>("moveSquare");
            Texture2D atkTex = content.Load<Texture2D>("atkSquare");
            Texture2D baseTex = content.Load<Texture2D>("baseSquare");
            Texture2D badBaseTex = content.Load<Texture2D>("badBaseSquare");
            Texture2D swordsmanSquare = content.Load<Texture2D>("swordsmanSquare");
            Texture2D badSwordsmanSquare = content.Load<Texture2D>("badSwordsmanSquare");
            Texture2D tankSquare = content.Load<Texture2D>("tankSquare");
            Texture2D badTankSquare = content.Load<Texture2D>("badTankSquare");
            explode = content.Load<Texture2D>("explosionSquare");
            baseDestroyed = content.Load<SoundEffect>("baseDestroyed");
            unitTakeDamage = content.Load<SoundEffect>("unitTakeDamage");
            unitDestroyed = content.Load<SoundEffect>("unitDestroyed");
            mainMusic = content.Load<SoundEffect>("mainMusic");
            music = mainMusic.CreateInstance();
            music.IsLooped = true;
            music.Play();

            //process map file for where to place units / obstacles
            StreamReader sr = new StreamReader("../../../../Content/" + game.mapToLoad + ".txt");
            String maxSizeString = sr.ReadLine();
            String[] maxSizes = maxSizeString.Split(',');
            int maxX = Convert.ToInt32(maxSizes[0]) * 32;
            int maxY = Convert.ToInt32(maxSizes[1]) * 32;

            while (!sr.EndOfStream)
            {
                String newUnitAdd = sr.ReadLine();
                String[] unitInfo = newUnitAdd.Split(',');
                int x = Convert.ToInt32(unitInfo[1]) * 32;
                int y = Convert.ToInt32(unitInfo[2]) * 32;
                switch (unitInfo[0])
                {
                    case "1A":
                        Archer newArcher = new Archer(new Vector2(x, y), true, archerSymbol, moveTex, atkTex, maxX, maxY);
                        myUnits.Add(newArcher);
                        allUnits.Add(newArcher);
                        break;
                    case "2A":
                        Archer newBadArcher = new Archer(new Vector2(x, y), false, badArcherSquare, moveTex, atkTex, maxX, maxY);
                        enemyUnits.Add(newBadArcher);
                        allUnits.Add(newBadArcher);
                        break;
                    case "1B":
                        Building newBase = new Building(new Vector2(x, y), true, baseTex);
                        myUnits.Add(newBase);
                        allUnits.Add(newBase);
                        break;
                    case "2B":
                        Building newBadBase = new Building(new Vector2(x, y), false, badBaseTex);
                        enemyUnits.Add(newBadBase);
                        allUnits.Add(newBadBase);
                        break;
                    case "1S":
                        Swordsman newSword = new Swordsman(new Vector2(x, y), true, swordsmanSquare, moveTex, atkTex, maxX, maxY);
                        myUnits.Add(newSword);
                        allUnits.Add(newSword);
                        break;
                    case "2S":
                        Swordsman newBadSword = new Swordsman(new Vector2(x, y), false, badSwordsmanSquare, moveTex, atkTex, maxX, maxY);
                        enemyUnits.Add(newBadSword);
                        allUnits.Add(newBadSword);
                        break;
                    case "1T":
                        Tank newTank = new Tank(new Vector2(x, y), true, tankSquare, moveTex, atkTex, maxX, maxY);
                        myUnits.Add(newTank);
                        allUnits.Add(newTank);
                        break;
                    case "2T":
                        Tank newBadTank = new Tank(new Vector2(x, y), false, badTankSquare, moveTex, atkTex, maxX, maxY);
                        enemyUnits.Add(newBadTank);
                        allUnits.Add(newBadTank);
                        break;
                    case "X":
                        Obstacle o = new Obstacle(new Vector2(x, y), true, null);
                        allUnits.Add(o);
                        break;
                }
                    

            }

            //if client, give control of red units and change turn state
            if (game.serverConnected)
            {
                List<Entity> tempEntities = myUnits;
                myUnits = enemyUnits;
                enemyUnits = tempEntities;
                foreach (Entity ent in allUnits)
                {
                    ent.owned = !ent.owned;
                }
                foreach (Entity ent in myUnits)
                {
                    ent.owned = true;
                }
                foreach (Entity ent in enemyUnits)
                {
                    ent.owned = false;
                }
                turnState = TurnState.enemyTurn;
            }
        }

        public void Update(GameTime gameTime)
        {
            renderer.Update(map, gameTime);

            //calculate where the camera should be moving
            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            var keyboardState = Keyboard.GetState();

            const float cameraSpeed = 500f;

            var moveDirection = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.W) || keyboardState.IsKeyDown(Keys.Up))
                moveDirection -= Vector2.UnitY;

            if (keyboardState.IsKeyDown(Keys.A) || keyboardState.IsKeyDown(Keys.Left))
                moveDirection -= Vector2.UnitX;

            if (keyboardState.IsKeyDown(Keys.S) || keyboardState.IsKeyDown(Keys.Down))
                moveDirection += Vector2.UnitY;

            if (keyboardState.IsKeyDown(Keys.D) || keyboardState.IsKeyDown(Keys.Right))
                moveDirection += Vector2.UnitX;

            //normalize the vector and move it if it is a nonzero vector
            if (moveDirection != Vector2.Zero)
            {
                moveDirection.Normalize();
                camera.Move(moveDirection * cameraSpeed * deltaSeconds);
            }

            //process end of turn, and notify other computer if necessary
            if (previousTurnState == TurnState.playerTurn && keyboardState.IsKeyDown(Keys.Enter))
            {
                endTurn();
                
                foreach (Entity ent in allUnits)
                {
                    ent.TurnReset();
                }
                
                if (game.clientConnected || game.serverConnected)
                {
                    // Do networking stuff
                    game.SendMessage("EndTurn");
                }
            }

            //determine if the game has ended
            hasOnlyMyBase = true;
            hasMyBase = false;
            foreach (Entity ent in myUnits)
            {
                if (ent is Building) hasMyBase = true;
                else hasOnlyMyBase = false;
            }

            hasOnlyEnemyBase = true;
            hasEnemyBase = false;
            foreach (Entity ent in enemyUnits)
            {
                if (ent is Building) hasEnemyBase = true;
                else { hasOnlyEnemyBase = false; }
            }

            if (!hasMyBase || hasOnlyMyBase)
            {
                Console.WriteLine("lose");
                music.Stop();
                game.CurrentState = GameState.lose;
            }

            if (!hasEnemyBase || hasOnlyEnemyBase)
            {
                Console.WriteLine("win");
                music.Stop();
                game.CurrentState = GameState.win;
            }

            previousTurnState = turnState;
            
        }

        //process end of turn
        public void endTurn()
        {
            if (turnState == TurnState.enemyTurn)
            {
                foreach(Entity e in enemyUnits)
                {
                    if(e is Building)
                    {
                        TurnOverIndicator ind = new TurnOverIndicator(e.getPosition());
                        tOList.Add(ind);
                        break;
                    }
                }
                turnState = TurnState.playerTurn;
            }
            else
            {
                foreach (Entity e in myUnits)
                {
                    if (e is Building)
                    {
                        TurnOverIndicator ind = new TurnOverIndicator(e.getPosition());
                        tOList.Add(ind);
                        break;
                    }
                }
                turnState = TurnState.enemyTurn;
            }
        }

        //process input from other computer
        public void GetInternetMessage(string message)
        {
            if (message.Equals("EndTurn"))
            {
                endTurn();
                return;
            }
            String[] array = message.Split(',');
            Entity ent = allUnits[Convert.ToInt32(array[0])];
            int x = Convert.ToInt32(array[2]);
            int y = Convert.ToInt32(array[3]);
            if (array[1] == "M")
            {
                ent.EnemyMove(x, y);
            }
            if (array[1] == "A")
            {
                ent.EnemyAttack(x, y, myUnits);
            }
            
        }

        public void Draw(GraphicsDevice graphicsDevice, GameTime gameTime)
        {
            graphicsDevice.Clear(Color.CornflowerBlue);

            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            graphicsDevice.RasterizerState = RasterizerState.CullNone;

            //get new camera position and draw it
            var viewMatrix = camera.GetViewMatrix();
            var projectionMatrix = Matrix.CreateOrthographicOffCenter(0, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height, 0, 0f, -1f);
            renderer.Draw(map, ref viewMatrix, ref projectionMatrix);
            spriteBatch.Begin();

            //have cpu do turn if enemy turn and no client / server connected
            if ((turnState == TurnState.enemyTurn) && (!game.clientConnected && !game.serverConnected))
            {
                cpu.Update(allUnits, myUnits, enemyUnits, turnState);
                System.Threading.Thread.Sleep(500);
                cpu.ResetRun();
                endTurn();
            }

            //process what units need to be modified if any
            List<Entity> tempList = new List<Entity>();
            foreach (Entity entity in allUnits)
            {
                String message = entity.Update(spriteBatch, camera, allUnits, myUnits, enemyUnits, turnState);
                if (message != "" && (game.clientConnected || game.serverConnected))
                {
                    message = allUnits.FindIndex(e => e.Equals(entity)) + "," + message;
                    game.SendMessage(message);
                }
                if (entity.GetHealth() <= 0)
                {
                    dthIlist.Add(new DeathIndicator(entity.getPosition()));
                    if (entity is Building)
                    {
                        baseDestroyed.Play();
                    }
                    if (entity is Unit)
                    {
                        unitDestroyed.Play();
                    }
                    if (myUnits.Contains(entity))
                    {
                        myUnits.Remove(entity);
                    }
                    if (enemyUnits.Contains(entity))
                    {
                        enemyUnits.Remove(entity);
                    }
                    tempList.Add(entity);
                }
                if (entity.TookDamage())
                {
                    dgIlist.Add( new DamageIndicator(entity.DamageTaken(), entity.getPosition()));
                    unitTakeDamage.Play();
                }
                
            }
            List<DamageIndicator> tmpDmg = new List<DamageIndicator>();
            foreach (DamageIndicator d in dgIlist)
            {
                if (!d.Update(font, spriteBatch, camera, gameTime.ElapsedGameTime))
                {
                    tmpDmg.Add(d);
                }
            }
            List<TurnOverIndicator> tmpTO = new List<TurnOverIndicator>();
            foreach (TurnOverIndicator t in tOList)
            {
                if (!t.Update(font, spriteBatch, camera, gameTime.ElapsedGameTime))
                {
                    tmpTO.Add(t);
                }
            }
            List<DeathIndicator> tmpDth = new List<DeathIndicator>();
            foreach (DeathIndicator d in dthIlist)
            {
                if (!d.Update(explode, spriteBatch, camera, gameTime.ElapsedGameTime))
                {
                    tmpDth.Add(d);
                }
            }
            if (tempList.Count != 0)
            {
                foreach (Entity e in tempList)
                {
                    allUnits.Remove(e);
                }
            }
            if (tmpDmg.Count != 0)
            {
                foreach (DamageIndicator d in tmpDmg)
                {
                    dgIlist.Remove(d);
                }
            }
            if (tmpDth.Count != 0)
            {
                foreach (DeathIndicator d in tmpDth)
                {
                    dthIlist.Remove(d);
                }
            }
            if (tmpTO.Count != 0)
            {
                foreach (TurnOverIndicator t in tmpTO)
                {
                    tOList.Remove(t);
                }
            }

            spriteBatch.End();
        }
    }
}
