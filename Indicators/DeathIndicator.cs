﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    public class DeathIndicator
    {
        private Vector2 position;
        private TimeSpan ts;

        public DeathIndicator(Vector2 position)
        {
            this.position = position;
            ts = TimeSpan.FromSeconds(1.2);
        }
        public bool Update(Texture2D tex, SpriteBatch batch, Camera2D cam, TimeSpan time)
        {
            if (ts > TimeSpan.Zero)
            {
                batch.Draw(tex, cam.WorldToScreen(position), Color.White);
                ts -= time;
                return true;
            }
            return false;
        }
    }
}
