﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.Indicators
{
    class TurnOverIndicator
    {
        private Vector2 position;
        private TimeSpan ts;

        public TurnOverIndicator(Vector2 position)
        {
            this.position = position;
            ts = TimeSpan.FromSeconds(1.2);
        }
        public bool Update(SpriteFont font, SpriteBatch batch, Camera2D cam, TimeSpan time)
        {
            if (ts > TimeSpan.Zero)
            {
                batch.DrawString(font, "Turn Over", cam.WorldToScreen(position), Color.OrangeRed);
                position.Y = position.Y - 1;
                ts -= time;
                return true;
            }
            return false;
        }
    }
}
