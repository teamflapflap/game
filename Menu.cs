﻿using System;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace game
{
    enum MenuState { Main, Connect, MapSelect, win, lose, Instructions, Instructions2}

    class Menu
    {
        private Texture2D mainMenuBG;
        private Texture2D mainMenuButton;
        private Texture2D instructionsBG;
        private Texture2D instructions2BG;
        private Texture2D loadMapBG;
        private Texture2D nextPageButton;
        private Texture2D previousPageButton;
        private Texture2D loadMapButton;
        private Texture2D instructionsButton;
        private Texture2D startGameButton;
        private Texture2D multiplayerButton;
        private Texture2D backButton;
        private Texture2D mpButton;
        private Texture2D connectMenuBG;
        private Texture2D connectButton;
        private Texture2D hostButton;
        private SpriteBatch spriteBatch;
        private Game1 game;
        EventHandler<TextInputEventArgs> onTextEntered;
        EventHandler<TextInputEventArgs> textInput;
        KeyboardState prevKeyState;
        ButtonState prevButtonState;
        public MenuState menuState;
        string ip;
        private SpriteFont font;
        private SoundEffect introMusic;
        private SoundEffectInstance introI;
        private Texture2D winBG;
        private Texture2D loseBG;
        private bool connectionFailed;
        private bool mapLoadFailed;

        public Menu(Game1 game)
        {
            this.game = game;
            menuState = MenuState.Main;
            ip = "";
            connectionFailed = false;
            mapLoadFailed = false;
        }

        public void Initialize()
        {
            game.Window.TextInput += TextEntered;
            onTextEntered += HandleInput;
            textInput += TextEntered;
        }

        //processes input from keyboard
        private void HandleInput(object sender, TextInputEventArgs e)
        {
            if(menuState == MenuState.Connect)
            {
                char charEntered = e.Character;
                if (charEntered == '\b' && ip.Length > 0)
                {
                    ip = ip.Substring(0, ip.Length - 1);
                }
                else if(charEntered !='\b')
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(ip);
                    sb.Append(charEntered.ToString());
                    ip = sb.ToString();
                }
            }
            if (menuState == MenuState.MapSelect)
            {
                char charEntered = e.Character;
                if (charEntered == '\b' && game.mapToLoad.Length > 0)
                {
                    game.mapToLoad = game.mapToLoad.Substring(0, game.mapToLoad.Length - 1);
                }
                else if (charEntered != '\b' && charEntered != '\r')
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(game.mapToLoad);
                    sb.Append(charEntered.ToString());
                    game.mapToLoad = sb.ToString();
                }
            }
        }

        //handles text events
        private void TextEntered(object sender, TextInputEventArgs e)
        {
            if (onTextEntered != null)
                onTextEntered.Invoke(sender, e);
        }

        public void StopMusic()
        {
            introI.Stop();
        }

        //declares the spriteBatch and buttons / ui elements / music that need to be loaded
        public void LoadContent(ContentManager content, GraphicsDevice graphicsDevice)
        {
            spriteBatch = new SpriteBatch(graphicsDevice);
            mainMenuBG = content.Load<Texture2D>("mainmenubg");
            mainMenuButton = content.Load<Texture2D>("mainMenuButton");
            loadMapBG = content.Load<Texture2D>("loadMapBG");
            instructionsButton = content.Load<Texture2D>("instructionsButton");
            loadMapButton = content.Load<Texture2D>("loadMapButton");
            multiplayerButton = content.Load<Texture2D>("multiplayerButton");
            startGameButton = content.Load<Texture2D>("startgamebutton");
            loadMapButton = content.Load<Texture2D>("loadmapbutton");
            font = content.Load<SpriteFont>("ArialFont");
            mpButton = content.Load<Texture2D>("multiplayerButton");
            connectMenuBG = content.Load<Texture2D>("connectMenuBg");
            connectButton = content.Load<Texture2D>("connectButton");
            hostButton = content.Load<Texture2D>("hostButton");
            backButton = content.Load<Texture2D>("backButton");
            introMusic = content.Load<SoundEffect>("introMenu");
            instructionsBG = content.Load<Texture2D>("instructionsBG");
            instructions2BG = content.Load<Texture2D>("instructions2BG");
            nextPageButton = content.Load<Texture2D>("nextPageButton");
            previousPageButton = content.Load<Texture2D>("previousPageButton");
            introI = introMusic.CreateInstance();
            introI.IsLooped = true;
            introI.Play();
            winBG = content.Load<Texture2D>("winBG");
            loseBG = content.Load<Texture2D>("loseBG1");
        }

        public void Update(GraphicsDevice graphicsDevice)
        {
            KeyboardState keyState = Keyboard.GetState();

            switch (menuState)
            {
                //events on map select screen
                case MenuState.MapSelect:
                    //if start game button pressed we switch gameState, music, and send message to client about which map to load if necessary
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 3 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        if (File.Exists("../../../../Content/" + game.mapToLoad + ".txt"))
                        {
                            mapLoadFailed = false;
                            introI.Stop();
                            menuState = MenuState.Main;
                            game.CurrentState = GameState.Play;
                            if (game.clientConnected) game.SendMessage("load," + game.mapToLoad);
                            game.mapToLoad = "";
                        }
                        else
                        {
                            mapLoadFailed = true;
                        }
                    }
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 535 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 430 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Main;
                    }
                    break;

                //events on main menu screen
                case MenuState.Main:
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 3 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Instructions;
                    }
                    //if loadMapButton is pressed we switch gameState to Play / dont do anything if client
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 2 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 2 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed && !game.serverConnected &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.MapSelect;
                    }
                    //go to multiplayer screen if multiplayer button pressed
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight * 2 / 3 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight * 2 / 3 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Connect;
                    }
                    break;

                //events on multiplayer screen
                case MenuState.Connect:
                    //start server if both client and server have not already started
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 3 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed && !game.serverStarted && !game.clientStarted &&
                        prevButtonState == ButtonState.Released)
                    {
                        game.StartServer();
                        game.serverStarted = true;
                    }
                    //start client if both client and server have not already started
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight * 2 / 3 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight * 2 / 3 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed && !game.clientStarted && !game.serverStarted &&
                        prevButtonState == ButtonState.Released)
                    {
                        connectionFailed = game.ConnectToServer(ip);
                        if (!game.serverConnected) connectionFailed = true;
                        if (!connectionFailed) game.clientStarted = true;
                    }
                    //go back to main menu screen if back button pressed
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 535 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 430 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Main;
                    }
                    break;

                //events on instructions screen
                case MenuState.Instructions:
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Main;
                    }
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 300 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 100  &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Instructions2;
                    }
                    break;

                //events on instructions2
                case MenuState.Instructions2:
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Main;
                    }
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 300 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Instructions;
                    }
                    break;
                
                //go back to main menu if button pressed and stop win / lose music if necessary
                case MenuState.win:
                case MenuState.lose:
                    if (Mouse.GetState().X < graphicsDevice.PresentationParameters.BackBufferWidth / 2 + 100 &&
                        Mouse.GetState().X > graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100 &&
                        Mouse.GetState().Y < graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 + 50 &&
                        Mouse.GetState().Y > graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50 &&
                        Mouse.GetState().LeftButton == ButtonState.Pressed &&
                        prevButtonState == ButtonState.Released)
                    {
                        menuState = MenuState.Main;
                        if (game.winI.State == SoundState.Playing) game.winI.Stop();
                        if (game.loseI.State == SoundState.Playing) game.loseI.Stop();
                        introI.Play();
                    }
                    break;
            }

            prevButtonState = Mouse.GetState().LeftButton;
            prevKeyState = keyState;
        }

        public void Draw(GraphicsDevice graphicsDevice)
        {
            //we set the bg color to white and draw all the buttons / ui elements with spriteBatch
            graphicsDevice.Clear(Color.White);
            spriteBatch.Begin();

            switch(menuState)
            {
                //what to draw on map select screen
                case MenuState.MapSelect:
                    spriteBatch.Draw(loadMapBG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(startGameButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50, 200, 100),
                        Color.White);
                    spriteBatch.Draw(backButton,
                           new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                           graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 435,
                           200, 100),
                           Color.White);
                    spriteBatch.DrawString(font, game.mapToLoad, new
                        Vector2(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 140,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 2 - 50), Color.Black);
                    if (mapLoadFailed)
                    {
                        spriteBatch.DrawString(font, "Invalid Map Name", new
                        Vector2(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 140,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50), Color.Black);
                    }
                    break;

                //what to draw on win screen
                case MenuState.win:
                    spriteBatch.Draw(winBG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(mainMenuButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50,
                        200, 100),
                        Color.White);
                    break;

                //what to draw on lose screen
                case MenuState.lose:
                    spriteBatch.Draw(loseBG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(mainMenuButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50,
                        200, 100),
                        Color.White);
                    break;

                //what to draw on instructions page
                case MenuState.Instructions:
                    spriteBatch.Draw(instructionsBG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(mainMenuButton,
                           new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 - 100,
                           graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50,
                           200, 100),
                           Color.White);
                    spriteBatch.Draw(nextPageButton,
                           new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 100 ,
                           graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50,
                           200, 100),
                           Color.White);
                    break;

                //what to draw on instructions2
                case MenuState.Instructions2:
                    spriteBatch.Draw(instructions2BG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(mainMenuButton,
                           new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 - 100,
                           graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50,
                           200, 100),
                           Color.White);
                    spriteBatch.Draw(previousPageButton,
                           new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 + 100,
                           graphicsDevice.PresentationParameters.BackBufferHeight / 5 - 50,
                           200, 100),
                           Color.White);
                    break;

                //what to draw on main menu screen
                case MenuState.Main:
                    spriteBatch.Draw(mainMenuBG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(instructionsButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50, 200, 100),
                        Color.White);
                    //hide load map button if client
                    if (!game.serverConnected) spriteBatch.Draw(loadMapButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 2 - 50,
                        200, 100),
                        Color.White);
                    spriteBatch.Draw(mpButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 2 / 3 - 50,
                        200, 100),
                        Color.White);
                    break;

                //what to draw on multiplayer screen
                case MenuState.Connect:
                    spriteBatch.Draw(startGameButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50, 200, 100),
                        Color.White);
                    spriteBatch.Draw(connectMenuBG,
                new Rectangle(0, 0, graphicsDevice.PresentationParameters.BackBufferWidth, graphicsDevice.PresentationParameters.BackBufferHeight),
                Color.White);
                    spriteBatch.Draw(connectButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 2 / 3 - 50,
                        200, 100),
                        Color.White);
                    spriteBatch.Draw(backButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 5 + 435,
                        200, 100),
                        Color.White);
                    spriteBatch.DrawString(font, ip, new 
                        Vector2(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 140,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 2 - 50), Color.Black);
                    //display whether or not connection has been established
                    if (game.clientConnected)
                    {
                        spriteBatch.DrawString(font, "Client Connected", new
                        Vector2(graphicsDevice.PresentationParameters.BackBufferWidth / 3 - 140,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50), Color.Black);
                    }
                    if (game.serverConnected)
                    {
                        spriteBatch.DrawString(font, "Server Connected", new
                        Vector2(graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 - 140,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50), Color.Black);
                    }
                    if (connectionFailed)
                    {
                        spriteBatch.DrawString(font, "Invalid IP", new
                        Vector2(graphicsDevice.PresentationParameters.BackBufferWidth * 2 / 3 - 140,
                        graphicsDevice.PresentationParameters.BackBufferHeight * 4 / 5 - 50), Color.Black);
                    }
                    spriteBatch.Draw(hostButton,
                        new Rectangle(graphicsDevice.PresentationParameters.BackBufferWidth / 2 - 100,
                        graphicsDevice.PresentationParameters.BackBufferHeight / 3 - 50,
                        200, 100),
                        Color.White);
                    break;
            }
            spriteBatch.End();
        }
    }
}
