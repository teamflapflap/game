# README #

This is a strategy game, similar to Civ or Settelers of Katan.

### How do I get my development environment configured? ###

* Make sure you have Visual Studio Installed
* Install Monogame v3.6
* Clone the repo to a folder.
* open the project and open it with Visual Studio

### How to Play ###

* Starting the game
	* If you want to do multiplayer:
		* Click "Multiplayer on the main screen.
		* If you are the host, click "Host"
		* If you are the client, type the IP of the host and click "Connect"
		* The client does not need to do anything else for the rest of the game starting phase.
	* Click "Load Map" to load a new map.
	* Type the name of the map you wish to load. options are:
		* Map1
		* Map2
		* Map3
		* Map4
		* Map5
	* Click "Start Game" to begin the game.
* Playing the game
	* Use WASD or arrow keys to move around.
	* When playing singleplayer, you control blue units.
	* When playing multiplayer, the host controls blue units and the client controls red units.
	* When playing singleplayer, you start first. When playing multiplayer, the host starts first.
	* You win when you destroy all enemy units, or the enemy base is destroyed.
* Controlling your units
	* On your turn, click on your units to select them, then move them if desired.
	* After you have finished your unit's movement, you can attack if desired.
	* Once you finish all movements and attacks with all units, press enter to end your turn.

### Contributors ###

* Elijah Potter
* Aliyah Wojtyna
* Brendan Fyan